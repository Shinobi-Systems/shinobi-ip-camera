# Shinobi-IP-Camera

> Tested on Raspberry Pi 4, Raspberry Pi OS
node-rtsp-rtmp-server (https://github.com/iizukanao/node-rtsp-rtmp-server) is used.

1. Enable `raspivid`.
2. Run the installer.
    ```
    sh INSTALL.sh
    ```

3. Start the app.
    ```
    pm2 start app.js
    ```

4. Open stream at `rtsp://RASPBERRY_IP_ADDRESS:80/live/STREAM0`.


> Run the following commands inside the folder of the source.

- View logs
    ```
    pm2 logs --lines 100
    ```
- Restart app
    ```
    pm2 restart app
    ```
- Stop app
    ```
    pm2 stop app
    ```


> Licensed Under MIT
https://opensource.org/licenses/MIT
