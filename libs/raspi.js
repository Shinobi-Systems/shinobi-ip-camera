module.exports = (s,config,io,app) => {
    //requires https://github.com/iizukanao/node-rtsp-rtmp-server running first
    const contentWriters = {}
    const contentWritersArray = []
    const spawn = require('child_process').spawn

    const raspiVid = spawn(`raspivid`,`-fps 25 -ex night -fli off -fl -md 4 -cd H264 -n -mm average -a 1036 -ae +25+25 -awb greyworld -t 0 -w 1920 -h 1080 -ih -pf high -g 25 -b 3000000 -drc high -rot 0 -o -`.split(' '))
    const ffmpeg = spawn(`ffmpeg`,`-hwaccel auto -i - -tune zerolatency -movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Poseidon Stream" -reset_timestamps 1 -c:v copy -an -crf 1 -g 1 -f mp4 -`.split(' '))
    raspiVid.stderr.on('data',(data) => {
        console.log(data.toString())
    })
    ffmpeg.stderr.on('data',(data) => {
        console.log(data.toString())
    })

    raspiVid.stdout.on('data',(data) => {
        ffmpeg.stdin.write(data)
    })
    ffmpeg.stdout.on('data',(data) => {
        contentWritersArray.forEach((action) => {
            if(action)action(data)
        })
    })
    // raspiVid.stdout.pipe(ffmpeg.stdin)
    app.get('/h264', function (req, res) {
        var date = new Date();
        res.writeHead(200, {
            'Date': date.toUTCString(),
            'Connection': 'keep-alive',
            'Cache-Control': 'private, no-cache, no-store, must-revalidate',
            'Expires': '-1',
            'Pragma': 'no-cache',
            'Content-Type': 'video/mp4',
            'Server': 'Shinobi H.264',
        })
        var ip = s.getClientIp(req)
        contentWriters[ip] = {
            index: contentWritersArray.length
        }
        contentWritersArray.push((buffer) => {
            res.write(buffer)
        })
        res.on('close', function () {
            const index = contentWriters[ip].index
            delete(contentWritersArray[index])
            delete(contentWriters[ip])
        })
    })
}
